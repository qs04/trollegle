package anon.trollegle;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Proxies implements Callback<UserConnection> {
    private Listener listener;
    private List<ProxyConfig> proxies = new ArrayList<>();
    @Persistent
    private long lastProxyChange = System.currentTimeMillis();
    @Persistent
    private long proxyLife = 2 * 60 * 60 * 1000;
    protected ProxyConfig proxy = new ProxyConfig(Proxy.NO_PROXY);
    { proxies.add(proxy); }
    
    public interface Listener {
        void proxyMessage(String message);
    }
    
    public Proxies(Listener listener) {
        this.listener = listener;
    }
    
    public ProxyConfig current() {
        return proxy;
    }
    
    public long getLife() { return proxyLife; }
    public void setLife(long life) { proxyLife = life; }
    
    public boolean switchIfStale() {
        synchronized (proxies) {
            if (System.currentTimeMillis() - lastProxyChange > proxyLife)
                return switchProxy();
        }
        return true;
    }
    
    public boolean isDirty() {
        return proxy.isDirty();
    }
    
    public StringBuilder startTorSearchesAndPingStatus(Collection<ProxyConfig> usedProxies) {
        StringBuilder message = startTorSearches(usedProxies);
        pingStatus(usedProxies);
        return message;
    }
    
    public StringBuilder startTorSearches(Collection<ProxyConfig> usedProxies) {
        boolean needSwitch = false;
        StringBuilder started = new StringBuilder();
        synchronized (proxies) {
            for (ProxyConfig proxy : proxies) {
                if (proxy.isTor() && (proxy.isBanned() || proxy.isDirty()) 
                        && !usedProxies.contains(proxy)
                        && proxy.isEnabled() && !proxy.isSearchingTor()) {
                    started.append(", " + proxy.getProxy());
                    proxy.startTorSearch(this);
                    if (proxy == this.proxy)
                        needSwitch = true;
                }
            }
            if (needSwitch)
                switchProxy();
        }
        return started;
    }
    
    public void pingStatus(Collection<ProxyConfig> usedProxies) {
        synchronized (proxies) {
            usedProxies.removeIf(proxy -> !proxies.contains(proxy));
        }
        usedProxies.removeIf(proxy -> !proxy.shouldPing());
        if (usedProxies.isEmpty()) return;
        new Thread() {
            public void run() {
                for (ProxyConfig proxy : usedProxies)
                    proxy.pingStatus();
            }
        }.start();
    }
    
    public void checkTorDuplicate(String address) {
        synchronized (proxies) {
            for (ProxyConfig proxy : proxies)
                if (proxy.canUse() && proxy.getExitAddress().equals(address))
                    throw new RuntimeException("nah");
        }
    }
    
    public void switchIfUnusable() {
        if (!proxy.canUse() || proxy.isDirty())
            switchProxy();
    }
    
    public int count() {
        synchronized (proxies) {
            return (int) proxies.stream().filter(ProxyConfig::canUse).count();
        }
    }
    
    public void add(String hostname, int port) {
        try {
            synchronized (proxies) {
                if (get(hostname, port) != null)
                    return;
                proxies.add(new ProxyConfig(new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(hostname, port))));
            }
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw e;
            throw new RuntimeException(e);
        }
    }
    
     public void addhttp(String hostname, int port) {
        try {
            synchronized (proxies) {
                if (get(hostname, port) != null)
                    return;
                proxies.add(new ProxyConfig(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostname, port))));
            }
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw e;
            throw new RuntimeException(e);
        }
    }
    

    public void addIfWorking(String hostname, int port) {
        try {
            ProxyConfig proxy;
            boolean existing;
            synchronized (proxies) {
                ProxyConfig existingProxy = get(hostname, port);
                if (existingProxy == null) {
                    proxy = new ProxyConfig(new Proxy(Proxy.Type.SOCKS, new InetSocketAddress(hostname, port)));
                    existing = false;
                } else {
                    proxy = existingProxy;
                    existing = true;
                }
            }
            addIfWorking(proxy, existing);
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw e;
            throw new RuntimeException(e);
        }
    }

     public void addhttpIfWorking(String hostname, int port) {
        try {
            ProxyConfig proxy;
            boolean existing;
            synchronized (proxies) {
                ProxyConfig existingProxy = get(hostname, port);
                if (existingProxy == null) {
                    proxy = new ProxyConfig(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostname, port)));
                    existing = false;
                } else {
                    proxy = existingProxy;
                    existing = true;
                }
            }
            addIfWorking(proxy, existing);
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw e;
            throw new RuntimeException(e);
        }
    }
    
    public void addIfWorking(ProxyConfig proxy, boolean existing) {
        if (existing && proxy.canUse()) {
            tellAdmin("Ignoring " + proxy.getProxy() + " (disable first to force check)");
            return;
        }
        proxy.startTorSearch((user, method, data) -> {
                if (method.equals("tor found")) {
                    if (existing) {
                        tellAdmin("Enabling: " + data);
                        setEnabled(proxy, true);
                    } else {
                        tellAdmin("Adding: " + data);
                        synchronized (proxies) {
                            proxies.add(proxy);
                        }
                    }
                    switchIfUnusable();
                } else if (method.equals("tor failed")) {
                    if (existing)
                        tellAdmin("Not enabling: " + data);
                    else
                        tellAdmin("Not adding: " + data);
                } else if (method.equals("tor duplicate")) {
                    checkTorDuplicate(data);
                }
            });
    }
    
    public void remove(String hostname, int port) {
        if (hostname.equals("0") && port == 0) {
            return;
        }
        ProxyConfig proxy = get(hostname, port);
        if (proxy == null)
            throw new RuntimeException("No such proxy!");
        synchronized (proxies) {
            proxies.remove(proxy);
        }
    }
    
    public void remove(String regex) {
        synchronized (proxies) {
            proxies.removeIf(a -> a.getProxy() != Proxy.NO_PROXY && a.matches(regex));
        }
    }
    
    public void clean() {
        synchronized (proxies) {
            proxies.removeIf(a -> a.getProxy() != Proxy.NO_PROXY && !a.isEnabled());
        }
    }
    
    ProxyConfig get(String[] address) {
        if (address == null || address.length != 2)
            return get("0", 0);
        return get(address[0], Integer.parseInt(address[1]));
    }
    
    protected ProxyConfig get(String hostname, int port) {
        if (hostname.equals("0")) {
            synchronized (proxies) {
                return proxies.stream().filter(a -> a.getProxy() == Proxy.NO_PROXY).findFirst().orElse(null);
            }
        } else {
            try {
                InetSocketAddress addr = new InetSocketAddress(hostname, port);
                synchronized (proxies) {
                    return proxies.stream().filter(a -> addr.equals(a.getProxy().address())).findFirst().orElse(null);
                }
            } catch (Exception e) {
                if (e instanceof RuntimeException)
                    throw e;
                throw new RuntimeException(e);
            }
        }
    }
    
    public void use(String hostname, int port) {
        ProxyConfig proxy = get(hostname, port);
        if (proxy == null)
            throw new RuntimeException("No such proxy!");
        this.proxy = proxy;
        lastProxyChange = System.currentTimeMillis();
    }

    public void setTorControl(String hostname, int port, int torport) {
        ProxyConfig proxy = get(hostname, port);
        if (proxy == null) {
            add(hostname, port);
            proxy = get(hostname, port);
        }
        if (proxy.getProxy() == Proxy.NO_PROXY)
            throw new RuntimeException("Direct connection as Tor not supported");
        if (torport == 0) {
            proxy.setTorControl(null);
        } else {
            proxy.setTorControl(new InetSocketAddress(hostname, torport));
            proxy.dirty();
        }
    }
    
    public void setEnabled(String regex, boolean on) {
        synchronized (proxies) {
            for (ProxyConfig proxy : proxies)
                if (proxy.matches(regex))
                    setEnabled(proxy, on);
        }
    }
    
    public void setEnabled(String hostname, int port, boolean on) {
        ProxyConfig proxy = get(hostname, port);
        setEnabled(proxy, on);
    }
    
    public void setEnabled(ProxyConfig proxy, boolean on) {
        if (proxy == null)
            throw new RuntimeException("No such proxy!");
        if (this.proxy == proxy && !on) {
            proxy.setEnabled(false);
            switchProxy();
        } else {
            if (on && proxy.isEnabled())
                proxy.unban();
            proxy.setEnabled(on);
        }
    }
    
    public ProxyConfig[] get() {
        synchronized (proxies) {
            return proxies.toArray(new ProxyConfig[proxies.size()]);
        }
    }
    
    public boolean switchProxy() {
        if (proxies.size() == 0) {
            tellAdmin("No proxies listed!");
            return false;
        }
        synchronized (proxies) {
            ProxyConfig candidate = Collections.max(proxies);
            if (!candidate.canUse()) {
                tellAdmin("All proxies are banned or disabled!");
                return false;
            }
            if (proxy == candidate) {
                return false;
            } else {
                lastProxyChange = System.currentTimeMillis();
            }
            proxy = candidate;
        }
        return true;
    }
    
    public boolean isBanned() {
        if (proxies.size() == 0)
            return false;
        synchronized (proxies) {
            return !Collections.max(proxies).canUse();
        }
    }
    
    public boolean handleBan(ProxyConfig bannedProxy) {
        synchronized (proxies) {
            bannedProxy.ban();
            if (bannedProxy != proxy)
                return true;
            return switchProxy();
        }
    }
    
    public boolean handleCaptcha(ProxyConfig bannedProxy) {
        synchronized (proxies) {
            bannedProxy.captcha();
            if (bannedProxy != proxy)
                return true;
            return switchProxy();
        }
    }
    
    public boolean shouldShowBans() {
        synchronized (proxies) {
            return proxies.stream().filter(ProxyConfig::isSearchingTor).count() < 3;
        }
    }
    
    public ProxyConfig getLogBotAuthAlt(ProxyConfig current) {
        synchronized (proxies) {
            return proxies.stream().filter(p -> p.canUse() && p.isTor() != current.isTor())
                    .findFirst().orElse(null);
        }
    }
    
    protected void tellAdmin(String message) {
        listener.proxyMessage(message);
    }
    
    @Override
    public void callback(UserConnection user, String method, String data) {
        if (method.equals("tor found")) {
            tellAdmin("Tor circuit found for: " + data);
            switchIfUnusable();
        } else if (method.equals("tor aborted")) {
            tellAdmin("Tor search aborted for: " + data);
        } else if (method.equals("tor failed")) {
            tellAdmin("Tor search failed for: " + data);
        } else if (method.equals("tor duplicate")) {
            checkTorDuplicate(data);
        } else {
            System.err.println("\n\n\nUNKNOWN CALLBACK METHOD in Proxies: '" + method + "'\n\n\n");
        }
    }
    
    private static final JsonSerializer<Proxies> serializer =
            new JsonSerializer<Proxies>(Proxies.class) {
        @Override
        void customSave(Proxies t, Map<String, Object> map) {
            if (t == null) return;
            synchronized (t.proxies) {
                map.put("proxies", t.proxies.stream()
                        .map(ProxyConfig::save).toArray(JsonValue[]::new));
                map.put("proxy", t.proxy.saveAddress());
            }
        }
        @Override
        void customLoad(Proxies t, Map<String, JsonValue> map) {
            if (t == null) return;
            JsonValue proxies = map.get("proxies"),
                selectedProxy = map.get("proxy");
            if (selectedProxy == null)
                selectedProxy = JsonValue.NULL;
            if (!JsonValue.isNull(proxies)) {
                synchronized (t.proxies) {
                    ProxyConfig direct = t.proxies.stream()
                            .filter(proxy -> proxy.getProxy() == Proxy.NO_PROXY)
                            .findFirst().orElse(null);
                    for (JsonValue spec : proxies.getList()) {
                        ProxyConfig proxy;
                        if (spec.getMap().get("type").toString().equals("DIRECT")) {
                            if (direct == null)
                                continue;
                            proxy = direct.load(spec);
                        } else {
                            proxy = t.makeProxy().load(spec);
                            t.proxies.add(proxy);
                        }
                        if (selectedProxy.equals(spec.getMap().get("address")))
                            t.proxy = proxy;
                    }
                }
            }
        }
    };
    
    public JsonValue save() {
        return serializer.save(this);
    }
    
    public Proxies load(JsonValue spec) {
        return serializer.load(this, spec);
    }
    
    /** Only here for overriding */
    protected ProxyConfig makeProxy() {
        return new ProxyConfig(Proxy.NO_PROXY);
    }
    
}