package anon.trollegle;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public abstract class CommonParser {

    protected String input;
    protected int pos;
    private static final Pattern numberPattern = Pattern.compile("-?(0|[1-9][0-9]*)(\\.[0-9]+)?([Ee](\\+|-)?[0-9]+)?");
    protected final Matcher numberMatcher;
    
    protected CommonParser(String input) {
        this.input = input;
        numberMatcher = numberPattern.matcher(input);
    }
    
    protected boolean end() {
        return pos >= input.length();
    }
    
    protected char peek() {
        return input.charAt(pos);
    }
    
    protected char consume() {
        return input.charAt(pos++);
    }
    
    protected boolean consumeChar(char expected) {
        if (!end() && input.charAt(pos) == expected) {
            pos++;
            return true;
        }
        return false;
    }
    
    protected boolean consumeString(String expected) {
        if (!end() && input.indexOf(expected, pos) == pos) {
            pos += expected.length();
            return true;
        }
        return false;
    }
    
    protected String consumeRegex(Matcher matcher) {
        if (!end() && matcher.region(pos, input.length()).lookingAt()) {
            pos = matcher.end();
            return matcher.group();
        }
        return null;
    }
    
    protected boolean skipSpace() {
        if (end()) return false;
        char c = peek();
        if (c == '\t' || c == '\n' || c == '\r' || c == ' ') {
            consume();
            return true;
        }
        return false;
    }
    
    protected void error() {
        throw new IllegalArgumentException();
    }
    
}
