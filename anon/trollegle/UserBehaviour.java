package anon.trollegle;

import anon.trollegle.MultiUser.LurkState;

import java.io.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import static anon.trollegle.Util.randomize;
import static anon.trollegle.Util.t;
import static anon.trollegle.Util.tn;

public class UserBehaviour extends Commands {
    
    protected Set<String> silentCommands;

    public UserBehaviour(Multi m) {
        super(m);
    }

    protected void addAll() {

        addCommand("help", 0, (args, target) -> {
            StringBuilder sb = new StringBuilder("Here are ")
                .append(randomize("", "all")).append(" the commands ").append(randomize("", "that "))
                .append(randomize("I accept", "this bot accepts")).append(".");

            for (Command c : commands.values()) {
                String helpstring = c.getHelpString(target);
                if (helpstring != null) {   // use null helpstring to hide a command
                    sb.append("\n").append(c.usage);
                    sb.append("\n    ");
                    sb.append(helpstring);
                }
            }
            sb.append("\n").append(t("sendcodes")).append("\n")
                .append(t("rejoin", MultiUser.MOTHERSHIP));

            target.schedTell(sb.toString());
        });
        setSilent("help");

        addCommand("invite", new Command(null, null) {
            public void process(String[] args, MultiUser target) {
                m.invite(target, args);
            }
            public String getHelpString(MultiUser target) {
                return "Adds another " + randomize("user", "stranger") + " to the chat (" +
                    100 * m.qfreq + "% " + randomize("recruited", "taken") + " from question mode)";
            }
        });
        
        addCommand("pm", 2, (args, target) -> m.pm(target, args[0], argsToString(1, args)));
        addCommand("me", 1, (args, target) -> m.me(target, argsToString(0, args)));
        addCommand("nick", 1, (args, target) -> m.nick(target, argsToString(0, args)));
        addCommand("list", 0, (args, target) -> {
            target.schedTell(t("listhead", m.listSize(target.isMuted(), target.isLurker())));
            if (m.users.isEmpty())
                return;
            String list;
            synchronized (m.users) {
                int padLength = Collections.max(m.users, MultiUser.compareId).getNumber() / 10 + 1;
                String listFormat = t("listitem").replace("_PAD_", Integer.toString(padLength));
                list = String.join("\n", m.users.stream()
                        .filter(u -> m.showInList(target, u))
                        .map(u -> (CharSequence) String.format(listFormat,
                                u.getNumber(), u.getNick(), getUserFacingFlags(target, u)))
                        ::iterator);
            }
            if (!list.isEmpty()) {
                target.schedTell("\n" + list);
            }
        });
        setSilent("list");
        
        addCommand("kick", 1, (args, target) -> m.slashkick(target, argsToString(0, args)));
        addCommand("dontkick", 1, (args, target) -> m.slashdontkick(target, argsToString(0, args)));
        addCommand("pat", 1, (args, target) -> m.patUser(target, args[0]));
        addCommand("spam", new Command(null, null) {
            public void process(String[] args, MultiUser target) {
                m.spam(target);
            }
            public String getHelpString(MultiUser target) {
                return t("spam_help", m.joinFreq() / 1000, m.effectiveAutoJoin()
                        ? t("spam_help_on") : t("spam_help_off"));
            }
        });
        addCommand("nospam", 0, (args, target) -> m.nospam(target));
        addCommand("switchproxy", 0, (args, target) -> m.switchconnection(target));

        addCommand("heyadmin", 0, (args, target) -> m.heyadmin(target, argsToString(0, args)));
        
         addCommand("rules", 0, (args, target) -> {
            target.schedTell(m.getrulelist());
        });
        
        addCommand("ignore", 1, (args, target) -> m.ignore(target, argsToString(0, args)));
        addCommand("unignore", 1, (args, target) -> m.unignore(target, argsToString(0, args)));
        addCommand("ignorelist", 0, (args, target) -> {
            Collection<MultiUser> ignoredUsers = m.getIgnored(target);
            if (ignoredUsers != null) {
                int count = ignoredUsers.size();
                target.schedTell(t(count == 1 ? "ignoreheadone" : "ignorehead", count));
                int padLength = Collections.max(ignoredUsers, MultiUser.compareId).getNumber() / 10 + 1;
                String format = t("listitem").replace("_PAD_", Integer.toString(padLength));
                String ignoreList = String.join("\n", ignoredUsers.stream()
                        .map(u -> (CharSequence) String.format(format, u.getNumber(), u.getNick(), ""))::iterator);
                target.schedTell("\n" + ignoreList);
            } else {
                target.schedTell(t("ignoreempty"));
            }
        });
        addCommand("clearignore", 0, (args, target) -> m.clearIgnore(target));
        setSilent("ignore", "unignore", "ignorelist", "clearignore");
        
        addCommand("8", 0, (args, target) -> target.schedTell(t("8reply", Util.minSec(target.age()), target.lurk())));
        addCommand("id", 0, (args, target) -> {
            if (args.length == 0)
                m.idSelf(target);
            else
                m.idOther(target, argsToString(0, args));
        });
        addCommand("showids", 0, (args, target) -> target.showNumbers(true));
        addCommand("hideids", 0, (args, target) -> target.showNumbers(false));
        addCommand("showidstwo", 0, (args, target) -> {
            target.setMessageStyle(MultiUser.MessageStyle.ID_SLIM);
            target.schedTell("Alternate dids currently displayed. (eg. [0 Admin])");
        });
        addCommand("bot", 0, (args, target) -> setOrRunBot(true, args, target));
        addCommand("unbot", 0, (args, target) -> setOrRunBot(false, args, target));
        addCommand("lurk", 0, (args, target) -> {
            if (target.isLurker()) {
                target.schedTell(t("lurk_already_lurking"));
            } else {
                target.schedTell(t("lurk_reset", Util.minSec(target.timeToLurk())));
                target.setLurkPreference(null);
            }
        });
        addCommand("nolurk", 0, (args, target) -> {
            if (!target.isLurker()) {
                if (target.getLurkPreference() == null)
                    target.schedTell(t("lurk_sticky"));
                else
                    target.schedTell(t("lurk_already_sticky"));
            }
            target.setLurkPreference(false);
        });
        setSilent("8", "id", "showids", "hideids", "bot", "unbot", "lurk", "nolurk");
        
        sanityCheck();
    }
    
    private Set<String> botAliases = new HashSet<>();
    
    {
        botAliases.add(t("bot"));
        botAliases.add(t("unbot"));
        for (String alias : t("bot_alias").split(" "))
            botAliases.add(alias);
        for (String alias : t("unbot_alias").split(" "))
            botAliases.add(alias);
    }
    
    void setOrRunBot(boolean value, String[] args, MultiUser target) {
        if (args.length == 0 || !args[0].startsWith("/")) {
            target.setBot(value);
            return;
        }
        if (args[0].length() > 1 && botAliases.contains(args[0].substring(1).toLowerCase(Locale.ROOT))) {
            target.schedTell(t("nocommand", args[0]));
            return;
        }
        boolean original = target.isBot();
        target.setBot(value);
        m.hear(target, argsToString(0, args));
        target.setBot(original);
    }
    
    public void tellNickChanged(MultiUser target, MultiUser topic, String oldNick, boolean silent) {
        if (shouldMute(target, topic) || !isPrivileged(target) && !target.isLurker() && topic.isLurker())
            return;
        if (!silent)
            target.schedTell(t("nickchange", oldNick, topic.getNick()));
        else if (topic == target)
            target.schedTell(t("silentnickchange", topic.getNick()));
    }
    
    public void tellJoined(MultiUser target, MultiUser topic) {
        String message = topic.getQuestion() == null || !m.checkBans(topic.getQuestion()).isEmpty()
                ? t("joinedtext", m.getDisplayNick(target, topic))
                : t("joinedquestion", m.getDisplayNick(target, topic), topic.getQuestion());
        target.schedTell(message);
    }
    
    public void tellLeft(MultiUser target, MultiUser topic) {
        tellLeft(target, topic, topic.getKickReason(), false);
    }
    
    public void tellLeft(MultiUser target, MultiUser topic, String reason, boolean showLurkers) {
        if (!isPrivileged(target)) {
            if (shouldMute(target, topic) && (
                    !m.countMuted || !m.checkBans("/nick " + topic.getNick()).isEmpty()))
                return;
            if (!showLurkers && !target.isLurker() && topic.isLurker())
                return;
        }
        if (reason == null)
            target.schedTell(t("left", m.getDisplayNick(target, topic)));
        else
            target.schedTell(t("leftreason", m.getDisplayNick(target, topic), reason));
    }
    
    public void updateLurk(MultiUser target, MultiUser topic) {
        LurkState lurkState = topic.lurkState();
        if (target == topic) {
            target.schedTell(t("lurk_" + lurkState));
            if (topic.shouldFamiliarizeLurker())
                target.schedTell(t("lurkhelp"));
        }
        if (isPrivileged(target)) {
            if (topic.getLurkPreference() == Boolean.TRUE)
                target.schedSend(t("lurk_admin_manual", m.getDisplayNick(target, topic)));
            else
                target.schedSend(t("lurk_admin_" + lurkState, m.getDisplayNick(target, topic)));
        } else if (m.showLurkChanges) {
            if (topic.getLurkPreference() == Boolean.TRUE)
                tellLeft(target, topic, null, true);
            else if (lurkState == LurkState.LURKER)
                tellLeft(target, topic, t("kickinactive"), true);
            else if (lurkState == LurkState.NORMAL)
                tellJoined(target, topic);
        }
        if (lurkState != LurkState.PRELURKER)
            updateFlags(target, topic, null);
    }
    
    public void updateFlagsPrivileged(MultiUser target, MultiUser topic, String message) {
        if (message != null && isPrivileged(target))
            target.schedSend(message);
    }
    
    public void updateFlags(MultiUser target, MultiUser topic, String message) {
        if (message != null)
            target.schedTell(message);
    }
    
    public String getUserFacingFlags(MultiUser target, MultiUser topic) {
        StringBuilder sb = new StringBuilder();
        if (target == topic)
            sb.append(t("listyou")).append(' ');
        if (topic.isLurker())
            sb.append(t("listlurker")).append(' ');
        if (m.ignores(target, topic))
            sb.append(t("listignored")).append(' ');
        return sb.toString();
    }
    
    protected boolean isPrivileged(MultiUser target) {
        // Scar from removing LogBot auth, kept in case anyone is overriding
        return m.isAdmin(target);
    }
    
    protected boolean shouldMute(MultiUser target, MultiUser topic) {
        if (!target.isAccepted())
            return true;
        if (topic == null)
            return false;
        return topic.isMuted() && !target.isMuted() || m.ignores(target, topic);
    }
    
    private static final String[] emptyArgs = {};
    
    public Command addCommand(String msgid, int arglen, Body body) {
        return addCommand(msgid, new BodyCommand(null, tn(msgid + "_help"), arglen, body));
    }
    
    public Command addCommand(String msgid, Command command) {
        String main = tn(msgid);
        if (main == null) {
            System.err.println("Untranslated command " + msgid);
            main = msgid;
        }
        String aliases = tn(msgid + "_alt");
        String usage = tn(msgid + "_usage");
        if (command.usage == null)
            if (usage == null)
                command.usage = "/" + main;
            else
                command.usage = "/" + main + " " + usage;
        return addCommand(main, command, aliases == null ? null : aliases.split(" "));
    }
    
    protected void setSilent(String msgid) {
        if (silentCommands == null)
            silentCommands = new HashSet<>();
        String main = tn(msgid);
        if (main == null) {
            main = msgid;
        }
        String aliases = tn(msgid + "_alt");
        silentCommands.add(main);
        if (aliases != null)
            for (String alias : aliases.split(" "))
                silentCommands.add(alias);
    }
    protected void setSilent(String... commands) {
        Util.forEach(commands, this::setSilent);
    }
    public boolean isSilent(String command) {
        return getCommand(command) == null || silentCommands != null && silentCommands.contains(command);
    }

    public void command(String cmd, MultiUser target) {
        String[] keyArgs = cmd.split(" ", 2);
        String key = keyArgs[0].toLowerCase(Locale.ROOT);

        String[] args = keyArgs.length == 1 ? emptyArgs : keyArgs[1].split(" ");
        Command command = getCommand(key);
        if (command != null) {
            try {
                command.process(args, target);
            } catch (RuntimeException e) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                pw.flush();
                target.schedTell(sw.toString());
                // Damn you, Java, for making this so tedious
            }
        } else if (key.length() > 0) {
            target.schedTell(t("nocommand", key));
        }
    }

}
