package anon.trollegle;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.ObjIntConsumer;

public class SimpleClient implements Callback<UserConnection>, Runnable {

    private boolean qShown;
    private boolean accepted;
    private UserConnection user;
    private Timer timer = new Timer(true);
    private TimerTask lurkTask;
    private boolean handoff;
    private String takeoverID;
    private String[] lurkMessages = {"/8"};
    private long lurkRate;
    private long lastLurk = System.currentTimeMillis();
    private ProxyConfig pc = new ProxyConfig(Proxy.NO_PROXY);
    private String[] args;
    
    private static long INACTIVE_HUMAN = 13 * 59 * 1000;

    public void callback(UserConnection user, String method, int data) {
        // nop
    }

    @Override
    public void callback(UserConnection user, String method, String data) {
        if (method.equals("captcha")) {
            log("captcha: " + data);
            accepted = false;
            user.dispose();
        } else if (method.equals("ban")) {
            log("ban: " + data);
            accepted = false;
            user.dispose();
        } else if (method.equals("died")) {
            log("died: " + data);
            accepted = false;
            user.dispose();
        } else if (method.equals("disconnected")) {
            log("Stranger has disconnected");
            accepted = true;
            user.dispose();
        } else if (method.equals("typing")) {
            // nop
        } else if (method.equals("stoppedtyping")) {
            // nop
        } else if (method.equals("message")) {
            hear(user, data);
        } else if (method.equals("connected") || method.equals("accepted")) {
            if (!qShown) {
                qShown = true;
                if (user.getQuestion() != null)
                    log("Question to discuss: " + user.getQuestion());
                else
                    log("Connected");
            }
        } else if (method.equals("pulse success")) {
            // nop
        } else if (method.equals("chatname")) {
            // nop
        } else if (method.equals("tor duplicate")) {
            // nop
        } else if (method.equals("tor found")) {
            run();
        } else {
            System.err.println("\n\n\nUNKNOWN CALLBACK METHOD '" + method + "'\n\n\n");
        }
    }
    
    public SimpleClient(String[] args) {
        this.args = args;
        try {
            for (String arg : args)
                if (arg.startsWith("-"))
                    command(arg.substring(1));
        } catch (Exception e) {
            if (e instanceof RuntimeException)
                throw (RuntimeException) e;
            throw new RuntimeException(e);
        }
        if (pc.getTorControl() != null) {
            pc.startTorSearch(this);
        }
    }
    
    
    public void run() {
        accepted = false;
        qShown = false;
        args = Arrays.stream(args).filter(a -> !a.startsWith("-")).toArray(String[]::new);
        user = new UserConnection(this) {
            @Override
            public void run() {
                if (takeoverID != null) {
                    handleReply(JsonValue.wrap(Util.mapHack("clientID", takeoverID)).toString());
                    connected();
                    new Poller().start();
                    handleEvents();
                } else if (handoff) {
                    log(establishChat());
                    System.exit(0);
                } else {
                    super.run();
                }
            }
            @Override
            public String getTopics() {
                return "&topics=" + JsonValue.wrap(args);
            }
            @Override
            public String spaceball(String in) {
                return in;
            }
            @Override
            public boolean sendAction(String action, String msg) {
                long thisLurk = System.currentTimeMillis();
                boolean success = super.sendAction(action, msg);
                if (success)
                    lastLurk = thisLurk;
                else
                    lastLurk += Math.max(0, INACTIVE_HUMAN - thisLurk + lastLurk);
                scheduleLurk();
                return success;
            }
        }.fill(args.length == 0, pc);
        user.start();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (user.isAlive()) {
            try {
                String cmd = in.readLine();
                if (cmd == null)
                    throw new EOFException();
                if (cmd.isEmpty())
                    continue;
                if (cmd.startsWith("/-"))
                    command(cmd.substring(2));
                else
                    say(cmd);
            } catch (EOFException e) {
                log("You have disconnected");
                if (user.isConnected()) {
                    user.sendDisconnect();
                    user.dispose();
                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            timer.purge();
            user.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void hear(UserConnection user, String data) {
        log("Stranger: " + data.replaceAll("  +", " "));
    }

    private void say(String data) {
        log("You: " + data);
        user.schedSend(data);
    }
    
    private void command(String data) throws Exception {
        String[] args = data.split("=|\\s+", 2);
        if (args[0].equals("help")) {
            log("-lurkrate=ms sets lurk rate (0 to disable)"
                    + "\n-lurkmsg=1|2 sets pipe-separated lurk messages (default /8)"
                    + "\n-proxy=host:port sets SOCKS proxy (only on startup)"
                    + "\n-torcontrol=host:port sets Tor control socket (only on startup)"
                    + "\n-handoff prints client id and exits (only on startup)"
                    + "\n-takeover=central1:blah takes an existing connection over (only on startup)"
                    + "\n-id prints client id (only while running)");
        } else if (args[0].equals("handoff")) {
            handoff = true;
        } else if (args[0].equals("id")) {
            if (user != null && user.isAlive())
                log(user.getID());
            log(user.killAndSave());
        } else if (args.length != 2) {
            log("Too few arguments");
        } else if (args[0].equals("lurkrate")) {
            long rate = Long.parseLong(args[1]);
            if (rate < 60)
                rate *= 60;
            if (rate < 3540)
                rate *= 1000;
            lurkRate = rate;
            scheduleLurk();
        } else if (args[0].equals("lurkmsg")) {
            lurkMessages = args[1].split("\\|");
        } else if (args[0].equals("takeover")) {
            takeoverID = args[1];
        } else if (args[0].equals("proxy")) {
            runProxyCommand(args[1], (host, port) -> {
                pc = new ProxyConfig(new Proxy(Proxy.Type.SOCKS,
                        new InetSocketAddress(host, port)));
            });
        } else if (args[0].equals("torcontrol")) {
            runProxyCommand(args[1], (host, port) -> 
                    pc.setTorControl(new InetSocketAddress(host, port)));
        }
    }
    
    private static void runProxyCommand(String arg, ObjIntConsumer<String> action) {
        String[] hostport = arg.split(":|\\s+", 2);
        if (hostport.length != 2)
            log("I need a port");
        else
            action.accept(hostport[0], Integer.parseInt(hostport[1]));
    }
    
    private synchronized void scheduleLurk() {
        if (lurkTask != null)
            lurkTask.cancel();
        if (lurkRate > 0) {
            lurkTask = new TimerTask() {
                @Override
                public void run() {
                    lurk();
                }
            };
            timer.schedule(lurkTask, Math.max(0, lurkRate + lastLurk - System.currentTimeMillis()));
        }
    }
    
    private void lurk() {
        say(String.format(Util.randomize(lurkMessages), System.currentTimeMillis() / 1000));
    }
    
    private static void log(Object message) {
        UserConnection.log(message);
    }
    
    public static void main(String[] args) {
        SimpleClient sc = new SimpleClient(args);
        if (sc.pc.getTorControl() == null) {
            sc.run();
            System.exit(sc.accepted ? 0 : 1);
        }
    }

}