package anon.trollegle;

public class SvetlanaMulti extends Multi {

    @Override
    protected MultiUser makeUser() {
        return new SvetlanaUser(this);
    }
    
    @Override
    public void command(MultiUser user, String data) {
        String[] ca = data.split(" ", 2);
        if (ca[0].equalsIgnoreCase("/help")) {
            user.schedTell("Available Commands: /rules || /name || /info || /me || /list || /pm");
        } else if (ca[0].equalsIgnoreCase("/info")) {
            user.schedTell("The purpose of this group chat is getting a clean chat under moderation as long as it's running. It basically connects you to different Omegle chats and lets you talk to more people. You may experience a slight lag, but nothing to worry about.");
            user.schedTell("To join this group again, use \"Svetlana\" as an interest.");
        } else {
            super.command(user, data);
        }
    }
    
    @Override
    public boolean addPulse() {
        synchronized (users) {
            for (MultiUser other : users)
                if (other.isPulse()) {
                    tellAdmin("Refusing to add more than one pulse! Try kicking the existing one first.");
                    return false;
                }
        }
        MultiUser uc = makeIdUser().fill(false, false, proxies.current());
        uc.pulse();
        synchronized (users) {
            uc.start();
            users.add(uc);
        }
        return true;
    }
    
    @Override
    protected void welcomeTriaged(MultiUser user, boolean isInformed) {
        boolean separateFirstLine = Math.random() > 0.66;
        String firstLine = "Welcome to an Omegle group chat!";
        if (!isInformed && separateFirstLine) {
            user.schedTell(firstLine);
        }

        if (spamSize() < 1) {
            user.schedTell("You've connected to the group chat as '" + user.getNick() + "'.");
            user.schedTell("(No other users are present.)");
            if (!murder) {
                add();
            }
        } else {
            StringBuilder otherUsers = welcomeNotifyAndCollect(user);
            boolean showUsers = false; //otherUsers.length() > 2 && Math.random() > 2d / 3;
            String countLine = "There are " + spamSize() + " other users online" + (showUsers ? ": " : ".");
            if (!separateFirstLine && !isInformed && Math.random() > 0.5) {
                user.schedTell(firstLine + countLine);
            } else {
                user.schedTell(countLine);
            }
            if (showUsers) {
                if (Math.random() > 0.5) {
                    user.schedTell("    " + otherUsers.substring(2));
                } else {
                    for (String otherUser : otherUsers.substring(2).split(", ")) {
                        user.schedTell("    " + otherUser);
                    }
                }
            }
            user.schedTell("You've connected to the group chat as '" + user.getNick() + "'.");
        }
        user.schedTell("Please, type /help to see a list of available commands and don't forget to read the /rules.");
    }
    
    @Override
    protected void inform(MultiUser user) {
        Util.sleep((int) (Math.random() * 700));

        if (Math.random() > 0.5)
            user.schedTell("Welcome to an Omegle group chat! If you wish to connect, please type in your desired nickname...");
        else
            user.schedTell("You're about to connect to a group chat on Omegle! Please type in your desired nickname...");
    }
    
    @Override
    protected boolean isEntryLine(String line) {
        return false;
    }
    
    @Override
    public void hearUntriaged(MultiUser user, String line) {
        if (line.length() > 3 && line.length() < 15 && !line.contains(" ")) {
            try {
                user.setNick(line);
                user.accept();
                welcome(user);
                return;
            } catch (Exception e) {
                // nothing
            }
        }
        super.hearUntriaged(user, line);
    }

}
